package com.ruoyi.record.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 来访人员对象 wy_visit_record
 * 
 * @author hdb
 * @date 2024-10-13
 */
public class WyVisitRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 入访序号 */
    private Long visitId;

    /** 入访人 */
    @Excel(name = "入访人")
    private String visitorName;

    /** 入访车牌 */
    @Excel(name = "入访车牌")
    private String visitorLicensePlate;

    /** 入访时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入访时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date visitTime;

    /** 入访手机号 */
    @Excel(name = "入访手机号")
    private String visitorPhoneNumber;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setVisitId(Long visitId) 
    {
        this.visitId = visitId;
    }

    public Long getVisitId() 
    {
        return visitId;
    }
    public void setVisitorName(String visitorName) 
    {
        this.visitorName = visitorName;
    }

    public String getVisitorName() 
    {
        return visitorName;
    }
    public void setVisitorLicensePlate(String visitorLicensePlate) 
    {
        this.visitorLicensePlate = visitorLicensePlate;
    }

    public String getVisitorLicensePlate() 
    {
        return visitorLicensePlate;
    }
    public void setVisitTime(Date visitTime) 
    {
        this.visitTime = visitTime;
    }

    public Date getVisitTime() 
    {
        return visitTime;
    }
    public void setVisitorPhoneNumber(String visitorPhoneNumber) 
    {
        this.visitorPhoneNumber = visitorPhoneNumber;
    }

    public String getVisitorPhoneNumber() 
    {
        return visitorPhoneNumber;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("visitId", getVisitId())
            .append("visitorName", getVisitorName())
            .append("visitorLicensePlate", getVisitorLicensePlate())
            .append("visitTime", getVisitTime())
            .append("visitorPhoneNumber", getVisitorPhoneNumber())
            .append("remarks", getRemarks())
            .toString();
    }
}
