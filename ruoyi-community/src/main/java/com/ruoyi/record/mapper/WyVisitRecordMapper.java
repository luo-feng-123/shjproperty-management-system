package com.ruoyi.record.mapper;

import java.util.List;
import com.ruoyi.record.domain.WyVisitRecord;

/**
 * 来访人员Mapper接口
 * 
 * @author hdb
 * @date 2024-10-13
 */
public interface WyVisitRecordMapper 
{
    /**
     * 查询来访人员
     * 
     * @param visitId 来访人员主键
     * @return 来访人员
     */
    public WyVisitRecord selectWyVisitRecordByVisitId(Long visitId);

    /**
     * 查询来访人员列表
     * 
     * @param wyVisitRecord 来访人员
     * @return 来访人员集合
     */
    public List<WyVisitRecord> selectWyVisitRecordList(WyVisitRecord wyVisitRecord);

    /**
     * 新增来访人员
     * 
     * @param wyVisitRecord 来访人员
     * @return 结果
     */
    public int insertWyVisitRecord(WyVisitRecord wyVisitRecord);

    /**
     * 修改来访人员
     * 
     * @param wyVisitRecord 来访人员
     * @return 结果
     */
    public int updateWyVisitRecord(WyVisitRecord wyVisitRecord);

    /**
     * 删除来访人员
     * 
     * @param visitId 来访人员主键
     * @return 结果
     */
    public int deleteWyVisitRecordByVisitId(Long visitId);

    /**
     * 批量删除来访人员
     * 
     * @param visitIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyVisitRecordByVisitIds(Long[] visitIds);
}
