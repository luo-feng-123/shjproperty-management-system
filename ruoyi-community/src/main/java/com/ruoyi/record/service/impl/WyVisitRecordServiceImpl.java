package com.ruoyi.record.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.record.mapper.WyVisitRecordMapper;
import com.ruoyi.record.domain.WyVisitRecord;
import com.ruoyi.record.service.IWyVisitRecordService;

/**
 * 来访人员Service业务层处理
 * 
 * @author hdb
 * @date 2024-10-13
 */
@Service
public class WyVisitRecordServiceImpl implements IWyVisitRecordService 
{
    @Autowired
    private WyVisitRecordMapper wyVisitRecordMapper;

    /**
     * 查询来访人员
     * 
     * @param visitId 来访人员主键
     * @return 来访人员
     */
    @Override
    public WyVisitRecord selectWyVisitRecordByVisitId(Long visitId)
    {
        return wyVisitRecordMapper.selectWyVisitRecordByVisitId(visitId);
    }

    /**
     * 查询来访人员列表
     * 
     * @param wyVisitRecord 来访人员
     * @return 来访人员
     */
    @Override
    public List<WyVisitRecord> selectWyVisitRecordList(WyVisitRecord wyVisitRecord)
    {
        return wyVisitRecordMapper.selectWyVisitRecordList(wyVisitRecord);
    }

    /**
     * 新增来访人员
     * 
     * @param wyVisitRecord 来访人员
     * @return 结果
     */
    @Override
    public int insertWyVisitRecord(WyVisitRecord wyVisitRecord)
    {
        return wyVisitRecordMapper.insertWyVisitRecord(wyVisitRecord);
    }

    /**
     * 修改来访人员
     * 
     * @param wyVisitRecord 来访人员
     * @return 结果
     */
    @Override
    public int updateWyVisitRecord(WyVisitRecord wyVisitRecord)
    {
        return wyVisitRecordMapper.updateWyVisitRecord(wyVisitRecord);
    }

    /**
     * 批量删除来访人员
     * 
     * @param visitIds 需要删除的来访人员主键
     * @return 结果
     */
    @Override
    public int deleteWyVisitRecordByVisitIds(Long[] visitIds)
    {
        return wyVisitRecordMapper.deleteWyVisitRecordByVisitIds(visitIds);
    }

    /**
     * 删除来访人员信息
     * 
     * @param visitId 来访人员主键
     * @return 结果
     */
    @Override
    public int deleteWyVisitRecordByVisitId(Long visitId)
    {
        return wyVisitRecordMapper.deleteWyVisitRecordByVisitId(visitId);
    }
}
