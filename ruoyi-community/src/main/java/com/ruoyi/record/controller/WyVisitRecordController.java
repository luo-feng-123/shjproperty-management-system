package com.ruoyi.record.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.record.domain.WyVisitRecord;
import com.ruoyi.record.service.IWyVisitRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 来访人员Controller
 * 
 * @author hdb
 * @date 2024-10-13
 */
@RestController
@RequestMapping("/community/record")
public class WyVisitRecordController extends BaseController
{
    @Autowired
    private IWyVisitRecordService wyVisitRecordService;

    /**
     * 查询来访人员列表
     */
    @PreAuthorize("@ss.hasPermi('community:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(WyVisitRecord wyVisitRecord)
    {
        startPage();
        List<WyVisitRecord> list = wyVisitRecordService.selectWyVisitRecordList(wyVisitRecord);
        return getDataTable(list);
    }

    /**
     * 导出来访人员列表
     */
    @PreAuthorize("@ss.hasPermi('community:record:export')")
    @Log(title = "来访人员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WyVisitRecord wyVisitRecord)
    {
        List<WyVisitRecord> list = wyVisitRecordService.selectWyVisitRecordList(wyVisitRecord);
        ExcelUtil<WyVisitRecord> util = new ExcelUtil<WyVisitRecord>(WyVisitRecord.class);
        util.exportExcel(response, list, "来访人员数据");
    }

    /**
     * 获取来访人员详细信息
     */
    @PreAuthorize("@ss.hasPermi('community:record:query')")
    @GetMapping(value = "/{visitId}")
    public AjaxResult getInfo(@PathVariable("visitId") Long visitId)
    {
        return success(wyVisitRecordService.selectWyVisitRecordByVisitId(visitId));
    }

    /**
     * 新增来访人员
     */
    @PreAuthorize("@ss.hasPermi('community:record:add')")
    @Log(title = "来访人员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WyVisitRecord wyVisitRecord)
    {
        return toAjax(wyVisitRecordService.insertWyVisitRecord(wyVisitRecord));
    }

    /**
     * 修改来访人员
     */
    @PreAuthorize("@ss.hasPermi('community:record:edit')")
    @Log(title = "来访人员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WyVisitRecord wyVisitRecord)
    {
        return toAjax(wyVisitRecordService.updateWyVisitRecord(wyVisitRecord));
    }

    /**
     * 删除来访人员
     */
    @PreAuthorize("@ss.hasPermi('community:record:remove')")
    @Log(title = "来访人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{visitIds}")
    public AjaxResult remove(@PathVariable Long[] visitIds)
    {
        return toAjax(wyVisitRecordService.deleteWyVisitRecordByVisitIds(visitIds));
    }
}
